# Project Gado

Project Gado is the authentication + authorization component for the Cibersys projects


## Requirements

- JDK 1.8+
- Leiningen 2+


## Usage

- Clone the project
- Install the required Environment see **Requirements**.
- Go to the root directory and run *lein ring server*


## License

Copyright © 2016 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
